#include "day-of-year.hpp"

int dayOfYear(int month, int dayOfMonth, int year) {
    if (month == 2) {
        dayOfMonth += 31;
        //start
    } else if (month == 3) {
        dayOfMonth += 59;
    } else if (month == 4) {
        dayOfMonth += 90;
    } else if (month == 5) {
        dayOfMonth += 120;
    } else if (month == 6) {
        dayOfMonth += 151;
    } else if (month == 7) {
        dayOfMonth += 181;
    } else if (month == 8) {
        dayOfMonth += 212;
    } else if (month == 9) {
        dayOfMonth += 243;
    } else if (month == 10) {
        dayOfMonth += 273;
    } else if (month == 11) {
        dayOfMonth += 304;
    } else if (month == 12) {
        dayOfMonth += 334;
    }
    if(year%4 == 0 && month>2) dayOfMonth ++;
    return dayOfMonth;
}

