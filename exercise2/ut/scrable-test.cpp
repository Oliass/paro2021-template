#include "gtest/gtest.h"
#include "scrable.hpp"

struct ScrableTestSuite {};


TEST(ScrableTestSuite, OneLetterWord)
{
  ASSERT_EQ(scramble_points("A"), 1);
}

TEST(ScrableTestSuite, MyCabbages)
{
  ASSERT_EQ(scramble_points("CABBAGE" ), 14);
}
